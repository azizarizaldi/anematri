<?php
defined('BASEPATH') or exit('No direct script access allowed');
class M_general extends CI_Model
{
	
	public function random_token()  {
		$alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abecdefhizklmnopqrstuvwxyz';
		$password = array(); 
		$alpha_length = strlen($alphabet) - 1; 
		for ($i = 0; $i < 12; $i++) 
		{
			$n = rand(0, $alpha_length);
			$password[] = $alphabet[$n];
		}
		return implode($password); 
	}
	
	public function replace_phone($phone="") {
        $separator = substr($phone,0,1);

        if($separator == 0) {
            $separator = "62".''.substr($phone,1);
        }
        else if($separator == 8) {
            $separator = "628".''.substr($phone,1);
        }
        else {
            $separator = $phone;
        }
        return $separator;
	}

	public function menu($id_role="") {
		$this->db->where("active","1");
		$this->db->where("id_role",$id_role);
		$this->db->order_by("sort_number","asc");
		return $this->db->get("ui_menu")->result();
	}

	public function master_referensi($type="") {
		$this->db->where("type",$type);
		return $this->db->get("master_referensi")->result();
	}

	public function getTableByID($table="",$id="") {
		$this->db->where("id",$id);
		return $this->db->get($table)->last_row();
	}

	public function ambilNamaBulan($bulan="") {
        switch ($bulan) {
            case 1:
                $bulan = "Januari";
                break;
            case 2:
                $bulan = "Februari";
                break;
            case 3:
                $bulan = "Maret";
                break;
            case 4:
                $bulan = "April";
                break;
            case 5:
                $bulan = "Mei";
                break;
            case 6:
                $bulan = "Juni";
                break;
            case 7:
                $bulan = "Juli";
                break;
            case 8:
                $bulan = "Agustus";
                break;
            case 9:
                $bulan = "September";
                break;
            case 10:
                $bulan = "Oktober";
                break;
            case 11:
                $bulan = "November";
                break;
            case 12:
                $bulan = "Desember";
                break;
            default:
                $bulan = Date('F');
                break;
        }
        return $bulan;
	}
}
