<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Model mail as helper for sending mail
 */
class M_mail extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Send email function
     *
     * @param mixed $target
     * @param mixed $subject
     * @param mixed $message
     * @return bool
     */

     public function send($target, $subject, $message)
    {
		// Konfigurasi email
		$config = [
			'mailtype'  => 'html',
			'charset'   => 'utf-8',
			'protocol'  => 'smtp',
			'smtp_host' => 'smtp.gmail.com',
			'smtp_user' => 'anematri@gmail.com',
			'smtp_pass' => 'deteksianemia',
			'smtp_crypto' => 'ssl',
			'smtp_port'   => 465,
			'crlf'    => "\r\n",
			'newline' => "\r\n"
		];

		// Load library email dan konfigurasinya
		$this->load->library('email', $config);
		$this->email->from('anematri@gmail.com', 'Admin Anematri');
		$this->email->to($target);
		$this->email->subject($subject);
		$this->email->message($message);

		return $this->email->send();
    }
}