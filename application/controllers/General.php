<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller {
/**
 *
 * @author Aziz Arif Rizaldi
 * @copyright 2019 - azizarifrizaldi@gmail.com
 * 
 */	

	public function changePassword() {
        $password_lama       = $this->input->post("password_lama");
        $password_baru       = $this->input->post("password_baru");
        $password_baru_ulang = $this->input->post("password_baru_ulang");
        $username            = $this->session->userdata("username");

        $this->db->where("username",$username);
		$checkUsers = $this->db->get("users");

        if($checkUsers->num_rows() > 0) {
            $getUser    = $checkUsers->last_row();
			if($getUser->status == '1') {
                if($password_baru_ulang == $password_baru) {
                    if (password_verify($password_lama, $getUser->password)) {
                        $this->db->where("id",$getUser->id);
                        $this->db->update("users",array("password" => password_hash($password_baru, PASSWORD_BCRYPT)));        
    
                        $response['status'] 	= true;
                        $response['message'] 	= "Silahkan logout untuk melanjutkan";    
    
                    }
                    else {
                        $response['status'] 	= false;
                        $response['message'] 	= "Password lama anda tidak sesuai!";    
                    }    
                }
                else {
                    $response['status'] 	= false;
                    $response['message'] 	= "Password baru tidak sama!";    
                }
            }
            else {
                $response['status'] 	= false;
                $response['message'] 	= "User tidak ditemukan!";
            }    
        }
		else {
			$response['status'] 	= false;
			$response['message'] 	= "User tidak ditemukan!";
		}

        echo json_encode($response);
		die;
    }

    public function userDetail() {
        $id = $this->input->get("id");

        $this->db->select("
            users.id,
            users.nama_lengkap,
            users.tempat_lahir,
            DATE_FORMAT(users.tanggal_lahir, '%d %b %Y') as tanggal_lahir,
            users.berat_badan,
            users.tinggi_badan,
            a.nama AS agama, 
            b.nama AS pendidikan,
        ");
        $this->db->join('master_referensi a',"a.id = users.ref_id_agama AND a.type = 'agama'","inner");
        $this->db->join('master_referensi b',"b.id = users.ref_id_pendidikan AND b.type = 'pendidikan'","inner");
        $this->db->where("users.id",$id);
        $getData = $this->db->get("users")->last_row();
        
        $data['identitas_pribadi'] = $getData;

        $this->db->select("users_detail.nama,users_detail.keterangan,a.nama as penghasilan , b.nama as pendidikan");
        $this->db->where("id_users",$id);
        $this->db->join('master_referensi a',"a.id = users_detail.ref_id_penghasilan AND a.type = 'penghasilan'","inner");
        $this->db->join('master_referensi b',"b.id = users_detail.ref_id_pendidikan AND b.type = 'pendidikan'","inner");    
        $data['identitas_ortu'] = $this->db->get("users_detail")->result();

        echo json_encode($data);
        die;
    }
    
    public function hasilAnamnesa() {
        $id = $this->input->get("id");
        $this->db->where("id",$id);
        $getData = $this->db->get("hasil_cek_anemia")->last_row();

        $this->db->select("kuisioner_gejala.nama as pertanyaan , jawaban");
        $this->db->where("id_users",$getData->id_users);
        $this->db->where("tanggal_isi",$getData->tanggal_cek);
        $this->db->join("kuisioner_gejala","kuisioner_gejala.id = hasil_kuisioner.id_kuisioner","inner");
        $kuisioner = $this->db->get("hasil_kuisioner")->result();

        echo json_encode($kuisioner);
        die;
    }

    public function grafikMonitoring() {
        $month   = date("m");
        $getData = $this->db->query('
            SELECT DATE_FORMAT(tanggal, "%d") AS hari , DATE_FORMAT(tanggal, "%d %b %Y") AS tanggal, COUNT(tanggal) AS jumlah
            FROM monitoring_rematri
            WHERE MONTH(monitoring_rematri.tanggal) = '.$month.'
            GROUP BY tanggal        
        ')->result();

        $tempCategories     = array();
        $tempData           = array();
        foreach($getData as $row) {
            $tempCategories[] = $row->tanggal;
            $tempData[]     = $row->jumlah;
        }
        $data['bulan']      = $this->M_general->ambilNamaBulan($month);
        $data['categories'] = json_encode($tempCategories,true);
        $data['data']       = json_encode($tempData,true);
        $data['showData']   = (count($tempData) == 0 ? 0:1);
        echo json_encode($data);
        die;
    }

    public function grafikAnemia() {
        $kategori_anemia = $this->db->get("kategori_anemia")->result();

        $temp = array();
        foreach($kategori_anemia as $row) {
            $this->db->where("id_kategori_anemia",$row->id);
            $tempData['jumlah'][]     = $this->db->get("hasil_cek_anemia")->num_rows();
            $tempData['nama'][]     = $row->nama;
        }

        $data['labels'] = json_encode($tempData['nama']);
        $data['series'] = json_encode($tempData['jumlah']);
        echo json_encode($data);
        die;

    }

    public function hapusRemaja() {
        $id     = $this->input->post("id");
        $data   = array();

        $this->db->where("id",$id);
        if($this->db->delete("users")) {
            $data['status']     = true;
            $data['message']    = "Data berhasil dihapus!";    
        }
        else {
            $data['status']     = false;
            $data['message']    = "Data gagal dihapus!";    
        }
        echo json_encode($data);
        die;
    }

    public function saveRemaja() {
        $post       = $this->input->post(null,true);

        $id         = $post["id"];
        $username   = $post["username"];

		$data		= array();
		$data['username']           = $username;
		$data['nama_lengkap']       = $post['nama_lengkap'];
		$data['tempat_lahir']       = $post['tempat_lahir'];
		$data['tanggal_lahir']      = date("Y-m-d", strtotime($post['tanggal_lahir']));
		$data['tinggi_badan']       = $post['tinggi_badan'];
		$data['berat_badan']        = $post['berat_badan'];
		$data['nohp']               = $post['nohp'];
		$data['ref_id_agama']       = $post['ref_id_agama'];
		$data['ref_id_pendidikan']  = $post['ref_id_pendidikan'];
		$data['ref_id_pendidikan']  = $post['ref_id_pendidikan'];
		$data['alamat']             = $post['alamat'];		
		$data['updated_at']	        = date("Y-m-d H:i:s");
        $this->db->where("id != ".$id);
        $this->db->where("username",$username);
        $checkUsers = $this->db->get("users")->num_rows();

        if($checkUsers > 0) {
            $data['status']     = false;
            $data['message']    = "Email ini sudah digunakan!";    
        }
        else {
            if(empty($id)) {
                $data['password']   = password_hash($post['password'], PASSWORD_BCRYPT);
                $data['id_role']    = 3;
                $data['status']     = 1;

                $this->db->insert("users",$data);
                $id_users           = $this->db->insert_id();                
                $data['status']     = true;
                $data['message']    = "Data berhasil disimpan!";        
            }

            else {
                $id_users           = $id; 
                $this->db->where("id",$id_users);
                $this->db->update("users",$data);        
                $data['status']     = true;
                $data['message']    = "Data berhasil diubah!";       
            }

            $this->db->where("id_users",$id_users);
            $this->db->where("keterangan","ayah");
            $checkAyah	= $this->db->get("users_detail")->num_rows();
    
            $data_ayah['nama'] 					= $post['nama_ayah'];
            $data_ayah['ref_id_pendidikan'] 	= $post['ref_id_pendidikan_ayah'];
            $data_ayah['ref_id_penghasilan'] 	= $post['ref_id_penghasilan_ayah'];
            $data_ayah['keterangan']		 	= 'ayah';
            $data_ayah['id_users']		 		= $id_users;
    
            if($checkAyah > 0) {
                $this->db->where("id_users",$id_users);
                $this->db->where("keterangan","ayah");	
                $this->db->update("users_detail",$data_ayah);
            }
            else {
                $this->db->insert("users_detail",$data_ayah);
            }
    
            $data_ibu['nama'] 					= $post['nama_ibu'];
            $data_ibu['ref_id_pendidikan'] 		= $post['ref_id_pendidikan_ibu'];
            $data_ibu['ref_id_penghasilan'] 	= $post['ref_id_penghasilan_ibu'];
            $data_ibu['keterangan']		 		= 'ibu';
            $data_ibu['id_users']		 		= $id_users;
    
            $this->db->where("id_users",$id_users);
            $this->db->where("keterangan","ibu");
            $checkIbu	= $this->db->get("users_detail")->num_rows();
    
            if($checkIbu > 0) {
                $this->db->where("id_users",$id_users);
                $this->db->where("keterangan","ibu");	
                $this->db->update("users_detail",$data_ibu);
            }
            else {
                $this->db->insert("users_detail",$data_ibu);
            }    
        }

        echo json_encode($data);
        die;
    }
}
