<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 * @author Aziz Arif Rizaldi
 * @copyright 2019 - azizarifrizaldi@gmail.com
 * 
 */

class Auth extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->twig->addGlobal('sessionData', $this->session->all_userdata());		

        if ($this->session->userdata('statusLogin') == '1' && $this->router->method != 'logout') {
            redirect(base_url('home'));
            exit();
        }
	}

	public function index() {
		$data['title']	= "Login";
		$this->twig->display('signin', $data);	
	}

	public function signup() {
		$data['title']	= "Gabung";
		$this->twig->display('signup', $data);	
	}

	public function saveSignup() {
		$post = $this->input->post(null,true);
		$username = $post['username'];
		$password = $post['password'];
		$nama_lengkap = $post['nama_lengkap'];
		$response = array();
		$this->db->where("username",$username);
		$checkUsers = $this->db->get("users")->num_rows();

		if(empty($checkUsers)) {
			$insert['username'] 	= $username;
			$insert['password'] 	= password_hash($password, PASSWORD_BCRYPT);
			$insert['nama_lengkap'] = $nama_lengkap;
			$insert['id_role'] 		= '3';
			$insert['created_at'] 	= date("Y-m-d H:i:s");
			
			$this->db->insert("users",$insert);
			$id_user 	= $this->db->insert_id();
			$token		= $this->M_general->random_token();

			$insert_verify['id_user'] 	= $id_user;
			$insert_verify['token'] 	= $token;
			$insert_verify['status'] 	= 0;
			$this->db->insert("users_verify",$insert_verify);

			$subject 	= "Konfirmasi Akun";
			$message 	= "
			<p>Hallo, <b>".$nama_lengkap."</b>! Untuk melakukan verifikasi akun, silahkan klik link dibawah ini ya ..</p>
			<a target='blank' href='".base_url()."auth/verify/".$token."'>Verifikasin Akun</a>";
			$sendData = $this->M_mail->send($username,$subject,$message);
	
			$response['status'] 	= true;
			$response['message'] 	= "Silahkan cek email kamu untuk melakukan verifikasi akun";
		}
		else {
			$response['status'] 	= false;
			$response['message'] 	= "Email sudah digunakan!";
		}

		echo json_encode($response);
		die;
	}

	public function verify($token="") {
		$this->db->where("token",$token);
		$checkVerify = $this->db->get("users_verify");

		if($checkVerify->num_rows() > 0) {
			$verify = $checkVerify->last_row();
			$data['status']		= 'success';
			$data['title']		= "Verifikasi Berhasil";
			$data['header']		= "Selamat, Verifikasi Berhasil 👍";
			$data['message']	= "Akun kamu telah berhasil diverifikasi ! Silahkan login untuk melanjutkan";

			$this->db->where("id",$verify->id);
			$this->db->update("users_verify",array("status" => "1"));

			$this->db->where("id",$verify->id_user);
			$this->db->update("users",array("status" => "1"));
		}
		else {
			$data['status']		= 'failure';
			$data['title']		= "Verifikasi Gagal";
			$data['header']		= "Oops, Verifikasi Gagal 😥";
			$data['message']	= "Silahkan hubungi admin untuk informasi lebih lanjut";
		}

		$this->twig->display('verify_message',$data);	
 	}

	public function loginProcess() {
		$post = $this->input->post(null,true);
		$username = $post['username'];
		$password = $post['password'];

		$response = array();

		$this->db->where("username",$username);
		$checkUsers = $this->db->get("users");

		if($checkUsers->num_rows() > 0) {
			$getUser = $checkUsers->last_row();
			if($getUser->status == '1') {
				if (password_verify($password, $getUser->password)) {

					$data['statusLogin']	= "1";
					$data['user_id']		= $getUser->id;
					$data['username']		= $username;
					$data['id_role']		= $getUser->id_role;
					$data['name']			= $getUser->nama_lengkap;
					$data['initialName']	= strtoupper(substr($getUser->nama_lengkap,0,1));
	
					$this->session->set_userdata($data);				
	
					$response['status'] 	= true;
					$response['message'] 	= "Selamat, kamu telah berhasil login!";							
				}	
				else {
					$response['status'] 	= false;
					$response['message'] 	= "Password yang kamu masukan salah ❌";							
				}	
			}
			else {
				$response['status'] 	= false;
				$response['message'] 	= "Akun kamu belum terverifikasi! ❌";	
			}
		}
		else {
			$response['status'] 	= false;
			$response['message'] 	= "User tidak ditemukan!";
		}

		echo json_encode($response);
		die;
	}

	public function logout() {
        $this->session->sess_destroy();
        redirect();
	}
}
