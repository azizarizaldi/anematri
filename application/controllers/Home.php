<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
/**
 *
 * @author Aziz Arif Rizaldi
 * @copyright 2019 - azizarifrizaldi@gmail.com
 * 
 */
	
	public function __construct() {
		parent::__construct();
        if (!$this->session->userdata('statusLogin')) {
            redirect(base_url('auth'));
            exit();
		}
		
		$id_users			= $this->session->userdata('user_id');
		$this->db->where("id_users",$id_users);
		$checkData = $this->db->get("users_detail")->num_rows();	

		$this->twig->addGlobal('sessionData', $this->session->all_userdata());		
		$this->twig->addGlobal('uiMenu', $this->M_general->menu($this->session->userdata('id_role')));		
		$this->twig->addGlobal('currentMenu','home');
		$this->twig->addGlobal('currentPage','home');
		$this->twig->addGlobal('checkData',$checkData);
	}

	public function index() {
		$id_users	= $this->session->userdata('user_id');
		$data['title']		= "Beranda";

		if($this->session->userdata('id_role') == '1') {

		}

		if($this->session->userdata('id_role') == '2') {
			$this->twig->display('components/home-petugas', $data);		
		}

		if($this->session->userdata('id_role') == '3') {			
			$this->db->select("DATE_ADD(tanggal_cek, INTERVAL 14 DAY) AS tanggal_cek_selanjutnya , DATEDIFF(DATE_ADD(tanggal_cek, INTERVAL 14 DAY),CURDATE()) AS sisa_hari");
			$this->db->where("id_users",$id_users);
			$checkData = $this->db->get("hasil_cek_anemia");

			$getData			= array();
			$checkHB			= $checkData->num_rows();
			$data['checkHB']	= $checkHB;

			if(!empty($checkHB)) {
				$curdate = date("Y-m-d");
				$getData = $checkData->last_row();
				$data['sisa_hari'] 					= $getData->sisa_hari;
				$data['tanggal_cek_selanjutnya'] 	= $getData->tanggal_cek_selanjutnya;
				$data['status'] 					= (strtotime($curdate) >= strtotime($getData->tanggal_cek_selanjutnya) ? '1':'0');
			}

			$this->twig->display('components/home', $data);		
		}
	}
	
	public function profile() {
		$this->twig->addGlobal('currentPage','profile');

		$id_users			= $this->session->userdata('user_id');
		
		$data['title']			= "Profile";
		$data['agama']			= $this->M_general->master_referensi('agama');
		$data['pendidikan']		= $this->M_general->master_referensi('pendidikan');
		$data['penghasilan']	= $this->M_general->master_referensi('penghasilan');
		$data['users']			= $this->M_general->getTableByID('users',$id_users);

		$this->db->where("id_users",$id_users);
		$this->db->where("keterangan","ayah");
		$data['data_ayah']		= $this->db->get("users_detail")->last_row();

		$this->db->where("id_users",$id_users);
		$this->db->where("keterangan","ibu");
		$data['data_ibu']		= $this->db->get("users_detail")->last_row();

		$this->twig->display('components/profile', $data);		
	}

	public function saveProfile() {
		$post 		= $this->input->post(null,true);
		$id_users	= $this->session->userdata('user_id');

		$data		= array();
		$data['nama_lengkap']       = $post['nama_lengkap'];
		$data['tempat_lahir']       = $post['tempat_lahir'];
		$data['tanggal_lahir']      = date("Y-m-d", strtotime($post['tanggal_lahir']));
		$data['tinggi_badan']       = $post['tinggi_badan'];
		$data['berat_badan']        = $post['berat_badan'];
		$data['nohp']               = $post['nohp'];
		$data['ref_id_agama']       = $post['ref_id_agama'];
		$data['ref_id_pendidikan']  = $post['ref_id_pendidikan'];
		$data['ref_id_pendidikan']  = $post['ref_id_pendidikan'];
		$data['alamat']             = $post['alamat'];		
		$data['updated_at']	= date("Y-m-d H:i:s");

		$this->db->where("id",$id_users);
		$this->db->update("users",$data);

		$this->db->where("id_users",$id_users);
		$this->db->where("keterangan","ayah");
		$checkAyah	= $this->db->get("users_detail")->num_rows();

		$data_ayah['nama'] 					= $post['nama_ayah'];
		$data_ayah['ref_id_pendidikan'] 	= $post['ref_id_pendidikan_ayah'];
		$data_ayah['ref_id_penghasilan'] 	= $post['ref_id_penghasilan_ayah'];
		$data_ayah['keterangan']		 	= 'ayah';
		$data_ayah['id_users']		 		= $id_users;

		if($checkAyah > 0) {
			$this->db->where("id_users",$id_users);
			$this->db->where("keterangan","ayah");	
			$this->db->update("users_detail",$data_ayah);
		}
		else {
			$this->db->insert("users_detail",$data_ayah);
		}

		$data_ibu['nama'] 					= $post['nama_ibu'];
		$data_ibu['ref_id_pendidikan'] 		= $post['ref_id_pendidikan_ibu'];
		$data_ibu['ref_id_penghasilan'] 	= $post['ref_id_penghasilan_ibu'];
		$data_ibu['keterangan']		 		= 'ibu';
		$data_ibu['id_users']		 		= $id_users;

		$this->db->where("id_users",$id_users);
		$this->db->where("keterangan","ibu");
		$checkIbu	= $this->db->get("users_detail")->num_rows();

		if($checkIbu > 0) {
			$this->db->where("id_users",$id_users);
			$this->db->where("keterangan","ibu");	
			$this->db->update("users_detail",$data_ibu);
		}
		else {
			$this->db->insert("users_detail",$data_ibu);
		}

		$response['status'] 	= true;
		$response['message'] 	= "Data berhasil disimpan!";

		echo json_encode($response);
		die;	
	}

	public function checkKuisioner() {
		$nomor = 1;

		$this->db->where("sort_number",$nomor);
		$getData = $this->db->get("kuisioner_gejala")->last_row();

		$response['nomor'] 				= $nomor;
		$response['pertanyaan'] 		= $getData->nama;
		
		echo json_encode($response);
		die;
	}

	public function saveKuisioner() {
		$totalKuisioner = $this->db->get("kuisioner_gejala")->num_rows();

		$nomor 	= $this->input->get("nomor");
		$params = $this->input->get("params");
	
		$id_users	= $this->session->userdata('user_id');
		
		$this->db->where("sort_number",$nomor);
		$getData = $this->db->get("kuisioner_gejala")->last_row();	

		$data['jawaban'] 		= $params;
		$data['id_users'] 		= $id_users;
		$data['id_kuisioner'] 	= $getData->id;
		$data['tanggal_isi'] 	= date("Y-m-d");
		$data['created_at'] 	= date("Y-m-d H:i:s");

		$this->db->where("id_users",$id_users);
		$this->db->where("id_kuisioner",$getData->id);
		$this->db->where("tanggal_isi",date("Y-m-d"));
		$checkJawaban = $this->db->get("hasil_kuisioner")->num_rows();

		if(empty($checkJawaban)) {
			$this->db->insert("hasil_kuisioner",$data);
		}
		else {
			$this->db->where("id_users",$id_users);
			$this->db->where("id_kuisioner",$getData->id);
			$this->db->where("tanggal_isi",date("Y-m-d"));	
			$this->db->update("hasil_kuisioner",$data);
		}
		$nomor++;

		if($nomor > $totalKuisioner) {
			$response['finish'] = 1;
		}

		else {
			$response['finish'] = 0;
			$this->db->where("sort_number",$nomor);
			$getData = $this->db->get("kuisioner_gejala")->last_row();	
			$response['pertanyaan'] 		= $getData->nama;	
		}

		$response['nomor'] 	= $nomor;		
		
		echo json_encode($response);
		die;
	}

	public function saveHasil() {
		$hasil_cek_hb = $this->input->post('hasil_cek_hb');

		if(!is_numeric($hasil_cek_hb)) {
			$response['status'] 	= false;
			$response['message'] 	= "silahkan masukan data yang benar!";
			$response['token'] 		= "";
		}
		else {
			$checkAnemia = $this->db->query("
				SELECT *
				FROM kategori_anemia
				WHERE '".$hasil_cek_hb."' BETWEEN kategori_anemia.min AND kategori_anemia.max 		
			");

			$response = array();
			if($checkAnemia->num_rows() > 0) {
				$getAnemia 		= $checkAnemia->last_row();
				$id_users		= $this->session->userdata('user_id');
				$tanggal_cek	= date("Y-m-d");
				$token			= $this->M_general->random_token();

				$data['id_users'] 				= $id_users;
				$data['hasil_cek_hb'] 			= $hasil_cek_hb;
				$data['id_kategori_anemia'] 	= $getAnemia->id;
				$data['tanggal_cek'] 			= date("Y-m-d");
				$data['token'] 					= $token;

				$this->db->where("id_users",$id_users);
				$this->db->where("tanggal_cek",$tanggal_cek);		
				$checkData = $this->db->get("hasil_cek_anemia")->num_rows();

				if(empty($checkData)) {
					$this->db->insert("hasil_cek_anemia",$data);
				}
				else {
					$this->db->where("id_users",$id_users);
					$this->db->where("tanggal_cek",$tanggal_cek);
					$this->db->update("hasil_cek_anemia",$data);
				}

				$response['status'] 	= true;
				$response['message'] 	= "Hasil cek HB berhasil disimpan!";
				$response['token'] 		= $token;

			}
			else {
				$response['status'] 	= false;
				$response['message'] 	= "silahkan masukan data yang benar!";
				$response['token'] 		= "";
			}
		}
		echo json_encode($response);
		die;
	}

	public function result($token="") {
		$this->db->select("hasil_cek_anemia.id , kategori_anemia.id as anemiaID, hasil_cek_anemia.hasil_cek_hb , kategori_anemia.nama , kategori_anemia.keterangan");
		$this->db->where("token",$token);
		$this->db->join("kategori_anemia","kategori_anemia.id = hasil_cek_anemia.id_kategori_anemia");
		$getData = $this->db->get("hasil_cek_anemia")->last_row();

		if(!empty($getData->id)) {
			$data['title']	= "Hasil Cek HB";
			$data['data']	= $getData;
			if($getData->anemiaID == '1') {
				$this->twig->display('components/result-anemia-normal', $data);			
			}
			if($getData->anemiaID == '2') {
				$this->twig->display('components/result-anemia-ringan', $data);			
			}
			if($getData->anemiaID == '3') {
				$this->twig->display('components/result-anemia-sedang', $data);			
			}
			if($getData->anemiaID == '4') {
				$this->twig->display('components/result-anemia-berat', $data);			
			}
		}
		else {
            redirect(base_url('auth'));
		}
	}
}
