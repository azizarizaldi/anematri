<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
/**
 *
 * @author Aziz Arif Rizaldi
 * @copyright 2019 - azizarifrizaldi@gmail.com
 * 
 */
    public function __construct() {
		parent::__construct();
        if (!$this->session->userdata('statusLogin') && $this->router->method != 'notfound') {
            redirect(base_url('auth'));
            exit();
		}
		
		$id_users			= $this->session->userdata('user_id');
		$this->db->where("id_users",$id_users);
		$checkData = $this->db->get("users_detail")->num_rows();	

		$this->twig->addGlobal('sessionData', $this->session->all_userdata());		
		$this->twig->addGlobal('uiMenu', $this->M_general->menu($this->session->userdata('id_role')));		
		$this->twig->addGlobal('currentMenu','page');
		$this->twig->addGlobal('currentPage','page');
        $this->twig->addGlobal('checkData',$checkData);
        
    }
    
    public function riwayatTandaGejala() {
        $this->twig->addGlobal('currentMenu','page/riwayatTandaGejala');

        $id_users	= $this->session->userdata('user_id');

        $this->db->select("hasil_cek_anemia.* ,kategori_anemia.nama, DATE_ADD(tanggal_cek, INTERVAL 14 DAY) AS tanggal_cek_selanjutnya , DATEDIFF(DATE_ADD(tanggal_cek, INTERVAL 14 DAY),CURDATE()) AS sisa_hari");
        $this->db->join("kategori_anemia","kategori_anemia.id = hasil_cek_anemia.id_kategori_anemia","inner");
        $this->db->where("id_users",$id_users);
        $this->db->order_by("tanggal_cek","desc");
        $data['getData']    = $this->db->get("hasil_cek_anemia")->result();    
        
        $data['title']		= "Riwayat Deteksi Anematri";
        $this->twig->display('components/riwayat-tanda-gejala', $data);		
    }

    public function monitoringRematri() {
        $this->twig->addGlobal('currentMenu','page/monitoringRematri');
        $id_users	= $this->session->userdata('user_id');

        $data['title']		= "Monitoring Asuhan Rematri";

        $this->db->where("id_users",$id_users);
        $this->db->order_by("tanggal","desc");
        $this->db->order_by("jam","desc");
        $data['getData']    = $this->db->get("monitoring_rematri")->result();
        $this->twig->display('components/monitoring-rematri', $data);		
    }

    public function saveMonitoring() {
        $post       = $this->input->post(null,true);
        $id_users	= $this->session->userdata('user_id');
        $tanggal    = strtotime($post['tanggal']);

        $id                 = $post['id'];

        $data['tanggal']    = date('Y-m-d',$tanggal);
        $data['jam']        = $post['jam'];
        $data['keterangan'] = $post['keterangan'];
        $data['id_users']   = $id_users;

        if(empty($id)) {
            $data['created_at'] = date("Y-m-d H:i:s");
    
            $processData = $this->db->insert("monitoring_rematri",$data);
    
            if($processData) {
                $response['status'] 	= true;
                $response['message'] 	= "Data berhasil di simpan!";
            }
            else {
                $response['status'] 	= false;
                $response['message'] 	= "Data gagal di simpan!";
            }
        }
        else {
            $data['updated_at'] = date("Y-m-d H:i:s");

            $this->db->where("id",$id);
            $processData = $this->db->update("monitoring_rematri",$data);
    
            if($processData) {
                $response['status'] 	= true;
                $response['message'] 	= "Data berhasil di simpan!";
            }
            else {
                $response['status'] 	= false;
                $response['message'] 	= "Data gagal di simpan!";
            }
        }
		echo json_encode($response);
		die;

    }

    public function getMonitoring() {
        $id = $this->input->get("id");
        $this->db->where("id",$id);
        $response  = $this->db->get("monitoring_rematri")->last_row();
		echo json_encode($response);
		die;
    }

    public function deleteMonitoring() {
        $id = $this->input->get("id");
        $this->db->where("id",$id);
        $processData  = $this->db->delete("monitoring_rematri");

        if($processData) {
            $response['status'] 	= true;
            $response['message'] 	= "Data berhasil di hapus!";
        }
        else {
            $response['status'] 	= false;
            $response['message'] 	= "Data gagal di hapus!";
        }

		echo json_encode($response);
		die;
    }

    public function riwayatAnemia() {
        $this->twig->addGlobal('currentMenu','page/riwayatAnemia');
        $data['title'] = "Riwayat Anemia Remaja";
        $this->db->select("
            users.id,
            users.nama_lengkap,
            users.tempat_lahir,
            users.tanggal_lahir,
            users.nohp,
            users.berat_badan,
            users.tinggi_badan,
            a.nama AS agama, 
            b.nama AS pendidikan,
            hasil_cek_anemia.hasil_cek_hb,
            kategori_anemia.nama AS tindak_lanjut,
            hasil_cek_anemia.tanggal_cek,
            hasil_cek_anemia.id as cekId               
        ");
        $this->db->join('kategori_anemia',"kategori_anemia.id = hasil_cek_anemia.id_kategori_anemia","inner");
        $this->db->join('users',"users.id = hasil_cek_anemia.id_users","inner");
        $this->db->join('master_referensi a',"a.id = users.ref_id_agama AND a.type = 'agama'","inner");
        $this->db->join('master_referensi b',"b.id = users.ref_id_pendidikan AND b.type = 'pendidikan'","inner");
        $this->db->order_by("hasil_cek_anemia.tanggal_cek","desc");
        $getData = $this->db->get("hasil_cek_anemia")->result();

        $data['getData'] = $getData;
        $this->twig->display('components/riwayat-anemia',$data);		
    }

    public function dataRemaja() {
		if($this->session->userdata('id_role') == '2') {
            $this->twig->addGlobal('currentMenu','page/dataRemaja');
            $data['title'] = "Remaja";
            $this->db->where("id_role","3");
            $data['getData'] = $this->db->get("users")->result();
            $this->twig->display('components/data-remaja',$data);		
        }
        else {
            $this->twig->display('not-found');		
        }
    }

    public function formRemaja($id="") {
		$data['agama']			= $this->M_general->master_referensi('agama');
		$data['pendidikan']		= $this->M_general->master_referensi('pendidikan');
		$data['penghasilan']	= $this->M_general->master_referensi('penghasilan');

		if($this->session->userdata('id_role') == '2') {
            $this->twig->addGlobal('currentMenu','page/dataRemaja');

            if($id == "") {
                $data['title'] = "Tambah Data";
                $this->twig->display('components/form-remaja',$data);		    
            }

            else {
                $id_users = $id;

                $data['users']			= $this->M_general->getTableByID('users',$id_users);
 
                if(!empty($data['users'])) {
                    $this->db->where("id_users",$id_users);
                    $this->db->where("keterangan","ayah");
                    $data['data_ayah']		= $this->db->get("users_detail")->last_row();
            
                    $this->db->where("id_users",$id_users);
                    $this->db->where("keterangan","ibu");
                    $data['data_ibu']		= $this->db->get("users_detail")->last_row();
            
                    $data['title'] = "Ubah Data";
                    $this->twig->display('components/form-remaja',$data);		    
                }
                else {
                    $this->twig->display('not-found');		
                }
            }
        }
        else {
            $this->twig->display('not-found');		
        }
    }

    public function notfound() {
        $this->twig->display('not-found');		
    }
}