(function ($) {
    "use strict";
    if ($("#chart-07").length) {
        var options = {
            chart: {
                height: 350,
                type: "radialBar"
            },
            plotOptions: {
                radialBar: {
                    startAngle: -90,
                    endAngle: 90,
                    dataLabels: {
                        name: {
                            show: false
                        },
                        value: {
                            offsetY: 0,
                            fontSize: "22px"
                        }
                    }
                }
            },
            fill: {
                type: "gradient",
                gradient: {
                    shade: "dark",
                    shadeIntensity: .15,
                    inverseColors: false,
                    opacityFrom: 1,
                    opacityTo: 1,
                    stops: [0, 50, 65, 91]
                }
            },
            stroke: {
                dashArray: 4
            },
            series: [67]
        };
        var chart = new ApexCharts(document.querySelector("#chart-07"), options);
        chart.render()
    }
    if ($("#chart-08").length) {
        var options = {
            chart: {
                height: 350,
                type: "bar"
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        position: "top"
                    }
                }
            },
            dataLabels: {
                enabled: true,
                formatter: function (val) {
                    return val + "%"
                },
                offsetY: -20,
                style: {
                    fontSize: "12px",
                    colors: ["#304758"]
                }
            },
            series: [{
                name: "Inflation",
                data: [2.3, 3.1, 4, 10.1, 4, 3.6, 3.2, 2.3, 1.4, .8, .5, .2]
            }],
            xaxis: {
                categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                position: "top",
                labels: {
                    offsetY: -18
                },
                axisBorder: {
                    show: false
                },
                axisTicks: {
                    show: false
                },
                crosshairs: {
                    fill: {
                        type: "gradient",
                        gradient: {
                            colorFrom: "#D8E3F0",
                            colorTo: "#BED1E6",
                            stops: [0, 100],
                            opacityFrom: .4,
                            opacityTo: .5
                        }
                    }
                },
                tooltip: {
                    enabled: true,
                    offsetY: -35
                }
            },
            fill: {
                gradient: {
                    shade: "light",
                    type: "horizontal",
                    shadeIntensity: .25,
                    gradientToColors: undefined,
                    inverseColors: true,
                    opacityFrom: 1,
                    opacityTo: 1,
                    stops: [50, 0, 100, 100]
                }
            },
            yaxis: {
                axisBorder: {
                    show: false
                },
                axisTicks: {
                    show: false
                },
                labels: {
                    show: false,
                    formatter: function (val) {
                        return val + "%"
                    }
                }
            },
            title: {
                text: "Monthly Tickets",
                floating: true,
                offsetY: 320,
                align: "center",
                style: {
                    color: "#444"
                }
            }
        };
        var chart = new ApexCharts(document.querySelector("#chart-08"), options);
        chart.render()
    }
    if ($("#chart-09").length) {
        var options = {
            colors: [colors[1]],
            chart: {
                height: 400,
                type: "bar"
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    endingShape: "rounded",
                    columnWidth: "25%"
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 1,
                colors: ["transparent"]
            },
            series: [
            {
                name: "Normal",
                data: [10,3,9,4]
            }
            ],
            xaxis: {
                categories: ["Normal", "Ringan", "Sedang", "Sedang", "Berat"]
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return val + " Remaja"
                    }
                }
            }
        };
        var chart = new ApexCharts(document.querySelector("#chart-09"), options);
        chart.render()
    }
})(window.jQuery);